package com.atguigu.gulimall.coupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/*
* 1.nacos的细节
*  命名空间：配置隔离；
*  配置集：public dev test  所有配置文件的集合
*   或者每个微服务之间配置命名隔离，在命名空间操作
*  配置集ID 类似  文件名
*  配置分组：DEFAULT_GROUP
*
* */
@MapperScan("com.atguigu.gulimall.coupon.dao")
@EnableDiscoveryClient
@SpringBootApplication
public class GulimallCouponApplication {
//测试
    public static void main(String[] args) {
        SpringApplication.run(GulimallCouponApplication.class, args);
    }

}
