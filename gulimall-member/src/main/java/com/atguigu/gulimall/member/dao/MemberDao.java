package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author chenshunu
 * @email sunlightcs@gmail.com
 * @date 2021-01-10 14:01:02
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
