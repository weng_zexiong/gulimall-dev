package com.atguigu.gulimall.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/*
* 1.远程调用
*    引入openfeign
*    编写一个接口，告诉SpringClound这个接口需要远程调用(声明接口的每个方法都是调用哪个远程服务的那个请求)
*    开启远程调用功能
*
*
*
*
* */

@MapperScan("com.atguigu.gulimall.member.dao")
@EnableFeignClients(basePackages="com.atguigu.gulimall.member.feign")
@EnableDiscoveryClient
@SpringBootApplication
public class GulimallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallMemberApplication.class, args);
    }

}
