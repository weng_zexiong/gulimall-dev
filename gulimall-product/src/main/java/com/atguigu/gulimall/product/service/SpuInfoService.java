package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.vo.SpuSaveVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2020-12-22 23:13:08
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /*
    * 商品上架
    * */
    void up(Long spuId);

    SpuInfoEntity getSpuInfoBySkuId(Long skuId);

    PageUtils queryPageByCondtion(Map<String, Object> params);

    void savesupInfo(SpuSaveVo vo);
}

